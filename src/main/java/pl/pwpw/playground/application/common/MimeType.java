package pl.pwpw.playground.application.common;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
public enum MimeType {
    PDF("application/pdf"),
    JPEG("image/jpeg"),
    JPG("image/jpg");

    public static final List<MimeType> ALLOWED_MIME_TYPES = List.of(PDF, JPEG, JPG);

    public static Boolean isValidMimeType(String mimeType) {
        return ALLOWED_MIME_TYPES.stream()
                .map(MimeType::getCode)
                .anyMatch(mimeType::equals);
    }

    private String code;
}
