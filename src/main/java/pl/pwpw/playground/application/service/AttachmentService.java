package pl.pwpw.playground.application.service;

import lombok.RequiredArgsConstructor;
import net.sf.jmimemagic.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import pl.pwpw.playground.application.common.MimeType;
import pl.pwpw.playground.application.dao.ApplicationDao;
import pl.pwpw.playground.application.dao.AttachmentDao;
import pl.pwpw.playground.application.entity.Application;
import pl.pwpw.playground.application.entity.Attachment;
import pl.pwpw.playground.application.exception.PlaygroundExceptionMessages;
import pl.pwpw.playground.application.exception.PlaygroundException;
import pl.pwpw.playground.application.mapper.AttachmentEntityMapper;

import java.io.IOException;
import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AttachmentService {

    private final AttachmentDao attachmentDao;
    private final ApplicationDao applicationDao;
    private final AttachmentEntityMapper attachmentEntityMapper;

    @Transactional
    public void addAttachmentToApplication(Long applicationId, MultipartFile attachment) {
        try {
            MagicMatch match = Magic.getMagicMatch(attachment.getBytes(), false);

            if (!MimeType.isValidMimeType(match.getMimeType())) {
                throw new PlaygroundException(PlaygroundExceptionMessages.ATTACHMENT_TYPE_ERROR);
            }

            Application application = Optional.ofNullable(applicationDao.findOne(applicationId))
                    .orElseThrow(() -> new PlaygroundException(PlaygroundExceptionMessages.APPLICATION_ID_NOT_PRESENT));

            Attachment attachmentEntity = attachmentEntityMapper.mapToApplicationDataDto(application, new Date(),
                    attachment.getOriginalFilename(), attachment.getBytes());

            attachmentDao.save(attachmentEntity);
        } catch (IOException | MagicException | MagicParseException | MagicMatchNotFoundException e) {
            throw new PlaygroundException(PlaygroundExceptionMessages.ATTACHMENT_ERROR);
        }
    }
}
