package pl.pwpw.playground.application.service;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.pwpw.playground.application.dao.ApplicationDao;
import pl.pwpw.playground.application.dto.ApplicationContactDto;
import pl.pwpw.playground.application.dto.ApplicationDataDto;
import pl.pwpw.playground.application.entity.Application;
import pl.pwpw.playground.application.exception.PlaygroundException;
import pl.pwpw.playground.application.exception.PlaygroundExceptionMessages;
import pl.pwpw.playground.application.mapper.ApplicationMapper;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ApplicationService {

    private final ApplicationDao applicationDao;
    private final ApplicationMapper applicationMapper;

    @Transactional
    public ApplicationContactDto getApplicationContactData(String applicationNumber) {
        Application application = Optional.ofNullable(applicationDao.getApplicationContactByApplicationNumber(applicationNumber))
                .orElseThrow(() -> new PlaygroundException(PlaygroundExceptionMessages.APPLICATION_NUMBER_NOT_PRESENT));

        return applicationMapper.mapToApplicationDataDto(application.getContactDetails());
    }

    @Transactional
    public ApplicationDataDto getApplicationDataDto(String email) {
        Application application = Optional.ofNullable(applicationDao.getApplicationDataByEmail(email))
                .orElseThrow(() -> new PlaygroundException(PlaygroundExceptionMessages.APPLICATION_EMAIL_NOT_PRESENT));

        return applicationMapper.mapToApplicationDataDto(application);
    }
}
