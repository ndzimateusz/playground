package pl.pwpw.playground.application.controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.pwpw.playground.application.dto.ApplicationContactDto;
import pl.pwpw.playground.application.dto.ApplicationDataDto;
import pl.pwpw.playground.application.service.ApplicationService;
import pl.pwpw.playground.application.service.AttachmentService;

import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping(value = "/application")
public class ApplicationController {

    private ApplicationService applicationService;
    private AttachmentService attachmentService;

    public ApplicationController(ApplicationService applicationService, AttachmentService attachmentService) {
        this.applicationService = applicationService;
        this.attachmentService = attachmentService;
    }

    @RequestMapping(value="/attachment", method = RequestMethod.POST)
    public Map<String, Boolean> addAttachment(@RequestParam("attachment") MultipartFile attachment,
                                              @RequestParam ("applicationId") Long applicationId) {
        attachmentService.addAttachmentToApplication(applicationId, attachment);
        return Collections.singletonMap("success", true);
    }

    @RequestMapping(value="/contact", method = RequestMethod.GET)
    public ApplicationContactDto getApplicationContact(@RequestParam ("applicationNumber") String applicationNumber) {
        return applicationService.getApplicationContactData(applicationNumber);
    }

    @RequestMapping(value="/data", method = RequestMethod.GET)
    public ApplicationDataDto getApplicationData(@RequestParam(value = "email") String email) {
        return applicationService.getApplicationDataDto(email);
    }

}

