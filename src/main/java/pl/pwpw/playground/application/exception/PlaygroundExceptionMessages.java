package pl.pwpw.playground.application.exception;

public class PlaygroundExceptionMessages {

    public static final String APPLICATION_ID_NOT_PRESENT = "Nie znaleziono wniosku o podanym id";
    public static final String ATTACHMENT_ERROR = "Wystąpił błąd podczas dodawania załącznika";
    public static final String ATTACHMENT_TYPE_ERROR = "Przesyłany załącznik musi być plikiem w formacie .pdf lub .jpg";
    public static final String APPLICATION_EMAIL_NOT_PRESENT = "Nie znaleziono wniosku, złożonego przez osobę o podanym adresie email";
    public static final String APPLICATION_NUMBER_NOT_PRESENT = "Nie znaleziono wniosku o podanym numerze";
}
