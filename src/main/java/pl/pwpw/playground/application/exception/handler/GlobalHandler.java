package pl.pwpw.playground.application.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import pl.pwpw.playground.application.exception.PlaygroundErrorResponse;
import pl.pwpw.playground.application.exception.PlaygroundException;


@ControllerAdvice
public class GlobalHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {PlaygroundException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public PlaygroundErrorResponse handle(PlaygroundException e) {
        PlaygroundErrorResponse response = new PlaygroundErrorResponse();
        response.setMsg(e.getMessage());
        return response;
    }


}
