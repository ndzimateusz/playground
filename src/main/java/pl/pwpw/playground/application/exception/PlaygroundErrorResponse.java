package pl.pwpw.playground.application.exception;

import lombok.Data;

@Data
public class PlaygroundErrorResponse {
    private String msg;
}
