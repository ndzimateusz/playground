package pl.pwpw.playground.application.exception;


public class PlaygroundException extends RuntimeException {

    public PlaygroundException(String messageKey) {
        super(messageKey);
    }

}
