package pl.pwpw.playground.application.dao.base;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;


@FunctionalInterface
public interface PredicateBuilder<T> {

    Predicate build (CriteriaBuilder cb, Root<T> root);
}
