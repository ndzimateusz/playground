package pl.pwpw.playground.application.dao;

import org.springframework.stereotype.Repository;
import pl.pwpw.playground.application.dao.base.AbstractDao;
import pl.pwpw.playground.application.entity.Attachment;

import java.util.Arrays;

@Repository
public class AttachmentDao extends AbstractDao<Attachment> {

    AttachmentDao() {
        super(Attachment.class);
    }

}
