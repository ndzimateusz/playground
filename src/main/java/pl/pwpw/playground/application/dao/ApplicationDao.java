package pl.pwpw.playground.application.dao;

import org.springframework.stereotype.Repository;
import pl.pwpw.playground.application.dao.base.AbstractDao;
import pl.pwpw.playground.application.dao.base.PredicateBuilder;
import pl.pwpw.playground.application.entity.Application;
import pl.pwpw.playground.application.entity.Application_;
import pl.pwpw.playground.application.entity.base.ApplicationNumber_;
import pl.pwpw.playground.application.entity.base.ContactDetails_;
import pl.pwpw.playground.application.entity.base.EmailAddress_;

import java.util.Arrays;

@Repository
public class ApplicationDao extends AbstractDao<Application> {

   ApplicationDao() {
      super(Application.class);
   }

   public Application getApplicationDataByEmail(String email) {
      PredicateBuilder<Application> emailPredicate = (cbuilder, root) ->
              cbuilder.equal(root.get(Application_.contactDetails).get(ContactDetails_.emailAddress).get(EmailAddress_.emailAddress), email);
      return findByPredicates(Arrays.asList(emailPredicate)).stream()
              .findFirst()
              .orElse(null);
   }

   public Application getApplicationContactByApplicationNumber(String applicationNumber) {
      PredicateBuilder<Application> emailPredicate = (cbuilder, root) ->
              cbuilder.equal(root.get(Application_.applicationNumber).get(ApplicationNumber_.applicationNumber), applicationNumber);
      return findByPredicates(Arrays.asList(emailPredicate)).stream()
              .findFirst()
              .orElse(null);
   }
}