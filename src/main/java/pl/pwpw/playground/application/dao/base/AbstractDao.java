package pl.pwpw.playground.application.dao.base;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public abstract class AbstractDao< T extends Serializable> {

   private Class<T>  clazz;

   @PersistenceContext
   EntityManager entityManager;

   protected AbstractDao( Class<T> clazz ) {
      this.clazz = clazz;
   }

   public T findOne( long id ){
      return entityManager.find( clazz, id );
   }


   protected List<T> findByPredicates(List<PredicateBuilder<T>> predicate) {
      CriteriaBuilder cb = entityManager.getCriteriaBuilder();
      CriteriaQuery<T> cq = cb.createQuery(clazz);
      Root<T> root = cq.from(clazz);
      TypedQuery<T> query = entityManager.createQuery(createWhereCriteriaQuery(cb, cq, root, predicate));
      return Optional.ofNullable(query.getResultList())
              .orElse(Collections.emptyList());
   }

   public void save(T entity){
      entityManager.persist(entity);
   }

   public T update(T entity){
      return entityManager.merge(entity);
   }

   public void delete(T entity){
      entityManager.remove(entity);
   }

   public void deleteById( long entityId ){
      T entity = findOne( entityId );
      delete( entity );
   }

   private CriteriaQuery<T> createWhereCriteriaQuery(CriteriaBuilder cb, CriteriaQuery<T> cq, Root<T> root, List<PredicateBuilder<T>> predicateBuilders) {
      List<Predicate> predicates = new ArrayList<>();

      for (PredicateBuilder<T> predicateBuilder : predicateBuilders) {
         predicates.add(predicateBuilder.build(cb, root));
      }

      return cq.where(predicates.toArray(new Predicate[0]));
   }
}