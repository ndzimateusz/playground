package pl.pwpw.playground.application.dto;

import lombok.Data;

@Data
public class ApplicationContactDto {

    private String phoneNumber;

    private String emailAddress;
}
