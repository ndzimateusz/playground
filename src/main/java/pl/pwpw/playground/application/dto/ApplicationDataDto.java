package pl.pwpw.playground.application.dto;


import lombok.Data;

@Data
public class ApplicationDataDto {
    private String applicationType;
    private String applicationNumber;
    private String firstName;
    private String lastName;
}
