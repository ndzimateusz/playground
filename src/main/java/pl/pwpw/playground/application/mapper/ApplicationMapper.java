package pl.pwpw.playground.application.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import pl.pwpw.playground.application.dto.ApplicationDataDto;
import pl.pwpw.playground.application.entity.Application;
import pl.pwpw.playground.application.entity.base.ContactDetails;
import pl.pwpw.playground.application.dto.ApplicationContactDto;

@Mapper(componentModel = "spring")
public interface ApplicationMapper {

    @Mapping(source = "emailAddress.emailAddress", target = "emailAddress")
    @Mapping(source = "phoneNumber.phoneNumber", target = "phoneNumber")
    ApplicationContactDto mapToApplicationDataDto(ContactDetails application);

    @Mapping(source = "applicationNumber.applicationNumber", target = "applicationNumber")
    @Mapping(source = "applicationType", target = "applicationType")
    @Mapping(source = "firstName", target = "firstName")
    @Mapping(source = "lastName", target = "lastName")
    ApplicationDataDto mapToApplicationDataDto(Application application);
}
