package pl.pwpw.playground.application.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import pl.pwpw.playground.application.entity.Application;
import pl.pwpw.playground.application.entity.Attachment;

import java.util.Date;

@Mapper(componentModel = "spring")
public interface AttachmentEntityMapper {

    @Mapping(source = "application", target = "application")
    @Mapping(source = "dateAdded", target = "dateAdded")
    @Mapping(source = "attachmentName", target = "attachmentName")
    @Mapping(source = "attachment", target = "attachment")
    Attachment mapToApplicationDataDto(Application application, Date dateAdded, String attachmentName, byte[] attachment);
}
