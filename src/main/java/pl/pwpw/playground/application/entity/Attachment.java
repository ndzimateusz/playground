package pl.pwpw.playground.application.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
public class Attachment implements Serializable {

    @Id
    @SequenceGenerator(name = "att_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "att_id_seq")
    @Column(name = "ATTACHMENT_ID")
    private Long attachmentId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "APPLICATION_ID")
    private Application application;

    @Column(name = "ATTACHMENT_NAME")
    private String attachmentName;

    @Column(name = "DATE_ADDED")
    private Date dateAdded;

    @Lob
    @Column(name = "ATTACHMENT", columnDefinition = "BLOB")
    private byte[] attachment;
}
