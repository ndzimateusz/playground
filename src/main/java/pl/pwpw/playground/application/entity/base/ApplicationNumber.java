package pl.pwpw.playground.application.entity.base;

import lombok.Getter;

import javax.persistence.Embeddable;

/**
 *
 */
@Embeddable
@Getter
public class ApplicationNumber {
    private String applicationNumber;
}
