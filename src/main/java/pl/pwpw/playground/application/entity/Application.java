package pl.pwpw.playground.application.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pwpw.playground.application.entity.base.ApplicationNumber;
import pl.pwpw.playground.application.entity.base.ApplicationType;
import pl.pwpw.playground.application.entity.base.ContactDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Data
@NoArgsConstructor
@Entity
public class Application implements Serializable {
    @Id
    @SequenceGenerator(name = "app_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app_id_seq")
    private Long appId;
    @Embedded
    private ApplicationNumber applicationNumber;
    private String firstName;
    private String lastName;
    @Embedded
    private ContactDetails contactDetails;
    @Enumerated(EnumType.STRING)
    private ApplicationType applicationType;

    @OneToMany(mappedBy = "application", cascade = CascadeType.PERSIST)
    private List<Attachment> attachments = new ArrayList<>();

}
